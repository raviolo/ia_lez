and(0,_,0).
and(_,0,0).
and(1,1,1).

or(_,1,1).
or(1,_,1).
or(0,0,0).

not(0,1).
not(1,0).

bigand([], 1).
bigand([A | L], O) :-
    and(A,Out,O),
    bigand(L, Out).

circuit(I1, I2, I3, O1, O2) :-
    and(I1,I2,P1),
    or(I2,I3,P2),
    not(P2, P3),
    and(I1, P3, O1),
    or(I2, P1, O2).
