:- use_module(library(clpfd)).

pianta(pomodoro).
pianta(basilico).
pianta(lattuga).
pianta(fragola).
pianta(cavolo).
pianta(granturco).
pianta(zucca).
pianta(fagiolo).
genere(cavolo, brassica).
famiglia(brassica, brassicacee).
consoc(pomodoro, basilico).
consoc(pomodoro, cavolo).
consoc(fragola, lattuga).
con_bi(X, Y) :-
    consoc(X, Y) ; consoc(Y, X).

% occorrenze(Piante, Occorrenze, Conta)
occorrenze([], Conta, Conta).
occorrenze([P | Piante], Occorrenze, Conta) :-
    (select(P=N, Conta, P=N1, A1), N1 is N+1 ->
	 occorrenze(Piante, Occorrenze, A1) ; occorrenze(Piante, Occorrenze, [P=1| Conta])).

requisiti(_, []).
requisiti([P=N | Occorrenze], Req) :-
    select(P=M, Req, R1), N >= M,
    requisiti(Occorrenze, R1).

requisiti(Piante, Occorrenze, Requisiti) :-
    occorrenze(Piante, Occorrenze, []),
    requisiti(Occorrenze, Requisiti).

% cons_score(Piante, Score)
cons_score([], 0).
cons_score([_], 0).
cons_score([P1, P2 | Rest], Score) :-
    pianta(P1), pianta(P2),
    cons_score([P2 | Rest], S),
    (con_bi(P1, P2) -> Score is S+1 ; Score = S).
