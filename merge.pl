:- use_module(library(clpfd)).

merge([], L, L).
merge(L, [], L).

merge([A | L1], [B | L2], [A | L3]) :-
    A #< B,
    merge(L1, [B | L2], L3).

merge([A | L1], [B | L2], [B | L3]) :-
    A #>= B,
    merge([A | L1], L2, L3).
