:- use_module(library(clpfd)).
:- consult(search).
% dimensione del livello, espressa in numero di colonne e righe
livello_dimenzionixy(8,8).
% i muri sono descritti con ?muro(X, Y, Lunghezza)
% muro orizzontale
hmuro(3, 4, 4).
% muro verticale
vmuro(4, 3, 4).
% la posizione della destinazione della scatola è p(X,Y)
deposito(p(2,7)).

% mostra le righe del livello partendo da quella con indice Y1,
% usando una rappresentazione ascii
mostra_livello(Soko-Box, W, H, Y1) :-
    Y1 #< H, Y #= Y1+1,
    livello_riga(Soko-Box, W, 0, Y, Row),
    string_chars(Str, Row),
    write(Str),
    mostra_livello(Soko-Box, W, H, Y).
% se non ho righe da mostrare, concludo con successo
mostra_livello(_, _, H, H).

% mostra in che stato è il livello, date le posizioni P1 e P2 di Sokoban e Box, con Pos = P1-P2
mostra_stato(Pos):-
    livello_dimenzionixy(W, H),
    mostra_livello(Pos, W, H, 0).

% mostra le caselle di una riga di una stanza, partendo da quella con indice X1
livello_riga(Soko-Box, W, X1, Y, [A, B | Rest]) :-
    X1 #< W, X #= X1+1,
    Pos = p(X,Y),    C = [A, B],
    (Pos = Soko   -> C = `:)` ;
     Pos = Box    -> C = `{}` ;
     muro(Pos)    -> C = `[]` ;
     deposito(Pos) -> C = `><` ; C = `.,`),
    
    livello_riga(Soko-Box, W, X, Y, Rest).
% se non ho altre caselle da mostrare, mostro la terminazione della riga '\n'
livello_riga(_, W, W, _, `\n`).

% Se c'è un muro verticale alla coordinata X e Y è tra inizio e fine muro, allora la posizione p(X,Y) è murata
muro(p(X,Y)) :-
    vmuro(X, Y1, L), YL #= Y1 + L - 1, between(Y1, YL, Y).

% Se c'è un muro orizzontale alla coordinata Y e X è tra inizio e fine muro, allora la posizione p(X,Y) è murata
muro(p(X,Y)) :-
    hmuro(X1, Y, L), XL #= X1 + L - 1, between(X1, XL, X).

% Se p(X,Y) rientra nella stanza e non è su un muro, allora ci possiamo passare
attraversabile(p(X,Y)) :-
    livello_dimenzionixy(W, H),
    between(1, W, X),
    between(1, H, Y),
    \+ muro(p(X,Y)).

direzione(s, p(0, 1)).
direzione(n, p(0, -1)).
direzione(e, p(1, 0)).
direzione(w, p(-1, 0)).
% direzione(se, p(1, 1)).
% direzione(ne, p(1, -1)).
% direzione(sw, p(-1, 1)).
% direzione(nw, p(-1, -1)).
somma_p(p(X0,Y0), p(X1,Y1), p(X,Y)) :-
    X0 + X1 #= X,
    Y0 + Y1 #= Y.

muovi(Dir, P1, P2) :-
    direzione(Dir, Dir_P),
    somma_p(P1, Dir_P, P2),
    attraversabile(P2).

manhattan_dist(p(X1, Y1)-p(X2, Y2), D) :- D #= abs(X1-X2) + abs(Y1-Y2).

% stato iniziale del livello, P1-P2, dove
% P1 è la posizione di Sokoban
% P2 è la posizione della scatola (Box)
% il "-" è presente solo per separare le due posizioni,
% poteva essere qualsiasi altro simbolo, come "/"
inizio(p(5,2)-p(6,5)).

% lo stato è finale se la scatola è nel deposito
fine(_-Box) :-
    deposito(Box).

prossimo(Prima, Dopo, 1) :-
    Prima = Ps-Pb, % posizione precedente di Sokoban e box
    Dopo = Soko-Box, % posizioni aggiornate dopo la mossa di Sokoban
    muovi(Dir, Ps, Soko),
    % se Soko si è spostato dove era la scatola, allora la sposta
    (Soko = Pb -> muovi(Dir, Pb, Box) ; Pb = Box).


stima(Soko-Box, V) :-
    findall(D, muovi(D, Box, _), L),
    length(L,N),
    deposito(Dep),
    manhattan_dist(Dep-Box, D),
    V #= D*4,
    mostra_stato(Soko-Box).
