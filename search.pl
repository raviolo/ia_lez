search_moves(Moves):-
    inizio(Start),
    stima(Start,Value), 
    search_goal([Value-n(top, Start, 0)], Final, [], Visited),
    construct_moves(Final, Visited, [], Moves).

merge([], L, L).
merge(L, [], L).

merge([A | L1], [B | L2], [A | L3]) :-
    val(A, N1), val(B, N2), N1 #< N2,
    merge(L1, [B | L2], L3).

merge([A | L1], [B | L2], [B | L3]) :-
    val(A, N1), val(B, N2), N1 #>= N2,
    merge([A | L1], L2, L3).

val(A-_, A).

next_n(n(_, S1, C1), n(S1, S2, C2)) :-
    prossimo(S1, S2, C),
    C1 + C #= C2.

finish_n(n(_, S, _)) :-
    fine(S).

estimate_n(n(_, S, C), V) :-
    stima(S, V1), V #= V1 + C.

search_goal([_-LastMove|_],LastMove, Visited, Visited) :-
    finish_n(LastMove).

search_goal([_-LastMove|Rest],Goal, Visited0, Visited) :-
    \+ finish_n(LastMove),
    (setof(Value-NextMove,
	   ( next_n(LastMove,NextMove),
	     \+ member(NextMove, Visited0),
	     \+ member(_-NextMove, Rest),
	     estimate_n(NextMove,Value)
	   ),
	   Children) ->
    merge(Children,Rest,NuovaFrontiera) ; NuovaFrontiera = Rest),
    search_goal(NuovaFrontiera,Goal,[LastMove|Visited0],Visited).

construct_moves(n(top,Start,0), _, Ms, [Start|Ms]).

construct_moves(n(P,Pos,C),Visited, Ms0, Ms):-
    member(n(GP,P,C1),Visited), next_n(n(GP,P,C1), n(P,Pos,C)),
    construct_moves(n(GP,P,C1),Visited,[Pos|Ms0],Ms).
