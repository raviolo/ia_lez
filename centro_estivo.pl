:- use_module(library(clpfd)).
% eta(Nome, Anni)
eta(giovanni, 10).
eta(luca, 12).
eta(bernardo, 14).
eta(federico, 17).
eta(anna, 18).
% amico(Nome, Nome)
amico(giovanni, luca).
amico(giovanni, bernardo).
% amico_bi(Nome, Nome)
% per avere una relazione di amicizia simmetrica
amico_bi(X, Y) :- amico(X, Y) ;  amico(Y, X).

% diverte(Nome, Gruppo, Quanto)
% <Nome> si diverte <Quanto> in <Gruppo>
diverte(R, Gruppo, poco) :-
    conta_amici(R, Gruppo, 0).

diverte(R, Gruppo, abbastanza) :-
    conta_amici(R, Gruppo, 1).

diverte(R, Gruppo, molto) :-
    conta_amici(R, Gruppo, N), N #> 1.

conta_amici(_, [], 0).
conta_amici(R, [A | Resto], N) :-
    N1 + 1 #= N,
    amico_bi(R,A),
    conta_amici(R, Resto, N1).

conta_amici(R, [A | Resto], N) :-
    \+ amico_bi(R,A),
    conta_amici(R, Resto, N).

% conta_amici(Nome, Gruppo, N, C)
% <Nome> ha <N-C> amici in <Gruppo>
% conta_amici(R, [A | Resto], N, C) :-
%     amico_bi(R, A),
%     C1 #= C + 1,
%     conta_amici(R, Resto, N, C1).
% 
% conta_amici(R, [A | Resto], N, C) :-
%     \+ amico_bi(R, A),
%     conta_amici(R, Resto, N, C).
% 
% conta_amici(_, [], C, C).

% gruppi(Ragazzi, Piccoli, Grandi)
% <Ragazzi> vengono suddivisi in <Piccoli> e <Grandi>, in base alla loro età
gruppi([R | Ragazzi], Piccoli, [R | Grandi]) :-
    eta(R, N), N #>= 16,
    gruppi(Ragazzi, Piccoli, Grandi).

gruppi([R | Ragazzi], [R | Piccoli], Grandi) :-
    eta(R, N), N #< 16,
    gruppi(Ragazzi, Piccoli, Grandi).

gruppi([], [], []).

% divertono(Gruppo, Divertono)
% per indicare l'i-esimo elemento di una lista L uso L[i]
% <Gruppo[i]> si diverte <Divertono[i]> in <Gruppo>
divertono(Ragazzi, Divertono) :-
    divertono_g(Ragazzi, Ragazzi, Divertono).

divertono_g([], _, []).
divertono_g([R | Ragazzi], Gruppo, [D | Divertono]) :-
    diverte(R, Gruppo, D),
    divertono_g(Ragazzi, Gruppo, Divertono).
    
